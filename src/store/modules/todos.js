import firebase from 'firebase/app';
import { v4 as uuidv4 } from 'uuid';

export default {
  namespaced: true,
  state: {
    priorities: ['low', 'normal', 'critical'],
    todos: [],
    loading: false,
    error: null,
    activeFilter: 'All',
    term: '',
    dateFilter: null
  },
  actions: {
    async addTodo({ commit }, payload) {
      try {
        commit('setLoading', true);

        const user = firebase.auth().currentUser;

        const todo = {
          id: uuidv4(),
          title: payload.title,
          description: payload.description,
          isDone: false,
          priority: payload.priority,
          category: payload.category,
          createDate: firebase.firestore.Timestamp.fromDate(new Date()),
          checkList: payload.checkList
        };

        const res = await firebase
          .firestore()
          .collection('todos')
          .doc(user.uid)
          .get();

        if (res.data()) {
          await firebase
            .firestore()
            .collection('todos')
            .doc(user.uid)
            .update({
              todos: [...res.data().todos, todo]
            });
        } else {
          await firebase
            .firestore()
            .collection('todos')
            .doc(user.uid)
            .set({
              todos: [todo]
            });
        }

        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async fetchTodosOnce() {
      const user = firebase.auth().currentUser;

      const res = await firebase
        .firestore()
        .collection('todos')
        .doc(user.uid)
        .get();

      return res.data().todos;
    },
    async toggleDone({ commit, dispatch }, payload) {
      try {
        commit('setLoading', true);

        const user = firebase.auth().currentUser;

        const todos = await dispatch('fetchTodosOnce');
        const index = todos.findIndex(todo => todo.id === payload);

        if (todos[index].isDone === false) {
          todos[index].isDone = true;
        } else {
          todos[index].isDone = false;
        }

        await firebase
          .firestore()
          .collection('todos')
          .doc(user.uid)
          .update({
            todos
          });

        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async editTodo({ commit, dispatch }, payload) {
      try {
        commit('setLoading', true);

        const user = firebase.auth().currentUser;

        const todos = await dispatch('fetchTodosOnce');
        const index = todos.findIndex(todo => todo.id === payload.todoId);
        if (payload.title) {
          todos[index].title = payload.title;
        }
        if (payload.description) {
          todos[index].description = payload.description;
        }

        await firebase
          .firestore()
          .collection('todos')
          .doc(user.uid)
          .update({
            todos
          });
        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async deleteTodo({ commit, dispatch }, payload) {
      try {
        commit('setLoading', true);

        const user = firebase.auth().currentUser;

        const todos = await dispatch('fetchTodosOnce');
        const newTodos = todos.filter(todo => todo.id !== payload);
        await firebase
          .firestore()
          .collection('todos')
          .doc(user.uid)
          .update({
            todos: newTodos
          });
        commit('setLoading', false);
      } catch (error) {
        commit('setLoading', false);
        commit('setError', error.message);
        throw error;
      }
    },
    async fetchTodos({ commit }) {
      commit('setLoading', true);
      const user = firebase.auth().currentUser;

      await firebase
        .firestore()
        .collection('todos')
        .doc(user.uid)
        .onSnapshot(doc => {
          if (doc.data()) {
            commit('setTodos', doc.data().todos);
          }
        });
      commit('setLoading', false);
    },
    async changeCheckListItem({ dispatch }, { todoId, itemId }) {
      const user = firebase.auth().currentUser;
      const todos = await dispatch('fetchTodosOnce');
      const todo = todos.find(item => item.id === todoId);
      console.log(todo);
      const todoCheckList = todo.checkList;
      const index = todoCheckList.findIndex(item => item.id === itemId);
      todoCheckList[index].isDone = !todoCheckList[index].isDone;

      await firebase
        .firestore()
        .collection('todos')
        .doc(user.uid)
        .update({
          todos
        });
    }
  },
  mutations: {
    setTodos(state, payload) {
      state.todos = payload;
    },
    setLoading(state, loading) {
      state.loading = loading;
    },
    setError(state, error) {
      state.error = error;
    },
    setTerm(state, payload) {
      state.term = payload;
    },
    setActiveFilter(state, payload) {
      state.activeFilter = payload;
    },
    setDateFilter(state, payload) {
      state.dateFilter = payload;
    }
  },
  getters: {
    todos(state) {
      const checkTerm = item => item.title.toLowerCase().indexOf(state.term.toLowerCase()) > -1;
      const checkDate = item => {
        const itemDate = item.createDate.seconds * 1000 + item.createDate.nanoseconds / 1000000;
        if (state.dateFilter[0] === state.dateFilter[1]) {
          return (
            new Date(itemDate).getDate() === new Date(state.dateFilter[0]).getDate() &&
            new Date(itemDate).getMonth() === new Date(state.dateFilter[0]).getMonth()
          );
        } else {
          return itemDate > state.dateFilter[0] - 86400000 && itemDate < state.dateFilter[1] + 86400000;
        }
      };
      const checkFilter = item => {
        if (state.activeFilter === 'Done') return item.isDone === true;
        else if (state.activeFilter === 'Progres') return item.isDone === false;
        else if (
          state.activeFilter &&
          state.activeFilter !== 'Progres' &&
          state.activeFilter !== 'Done' &&
          state.activeFilter !== 'All'
        )
          return item.category === state.activeFilter;
        else return state.todos;
      };
      if (state.dateFilter) {
        return state.todos.filter(item => checkTerm(item) && checkDate(item) && checkFilter(item)).reverse();
      } else {
        return state.todos.filter(item => checkTerm(item) && checkFilter(item)).reverse();
      }
    },
    loading(state) {
      return state.loading;
    },
    error(state) {
      return state.error;
    },
    term(state) {
      return state.term;
    },
    activeFilter(state) {
      return state.activeFilter;
    },
    dateFilter(state) {
      return state.dateFilter;
    }
  }
};
