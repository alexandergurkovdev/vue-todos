import firebase from 'firebase/app';

export default {
  namespaced: true,
  state: {
    userCategories: []
  },
  actions: {
    async fetchUserCategories({ commit }) {
      const user = firebase.auth().currentUser;

      await firebase
        .firestore()
        .collection('categories')
        .doc(user.uid)
        .onSnapshot(doc => {
          if (doc.data()) {
            commit('setUserCategories', doc.data().categories);
          }
        });
    },
    // eslint-disable-next-line no-unused-vars
    async addCategory({ context }, payload) {
      const user = firebase.auth().currentUser;

      const res = await firebase
        .firestore()
        .collection('categories')
        .doc(user.uid)
        .get();

      const index = res.data().categories.find(item => item.value === payload);

      if (!index) {
        const category = {
          value: payload
        };

        await firebase
          .firestore()
          .collection('categories')
          .doc(user.uid)
          .update({
            categories: [...res.data().categories, category]
          });
      }
    }
  },
  mutations: {
    setUserCategories(state, userCategories) {
      state.userCategories = userCategories;
    }
  },
  getters: {
    userCategories(state) {
      return state.userCategories;
    }
  }
};
