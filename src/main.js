import Vue from 'vue';
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import router from './router';
import store from './store';
import {
  Avatar,
  Autocomplete,
  Dialog,
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Input,
  Select,
  Option,
  OptionGroup,
  Button,
  Popover,
  Tooltip,
  Form,
  FormItem,
  Tag,
  Alert,
  Icon,
  Row,
  Col,
  Progress,
  Link,
  Divider,
  Image,
  Loading,
  Notification,
  Popconfirm,
  DatePicker,
  Checkbox
} from 'element-ui';
import dateFilter from './filters/date.filter';
import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
import 'element-ui/lib/theme-chalk/reset.css';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue';
Vue.use(Avatar);
Vue.use(Autocomplete);
Vue.use(Dialog);
Vue.use(Dropdown);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(Input);
Vue.use(Select);
Vue.use(Option);
Vue.use(OptionGroup);
Vue.use(Button);
Vue.use(Popover);
Vue.use(Popconfirm);
Vue.use(Tooltip);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Tag);
Vue.use(Alert);
Vue.use(Icon);
Vue.use(Row);
Vue.use(Col);
Vue.use(Progress);
Vue.use(Link);
Vue.use(Divider);
Vue.use(Image);
Vue.use(DatePicker);
Vue.use(Checkbox);
Vue.use(Loading.directive);
locale.use(lang);
Vue.filter('date', dateFilter);

Vue.prototype.$loading = Loading.service;
Vue.prototype.$notify = Notification;

Vue.config.productionTip = false;

const firebaseConfig = {
  apiKey: 'AIzaSyBO27KsI7JbTLlysvI1MqC8B2MwY9s5F1A',
  authDomain: 'vue-todos-6ca0a.firebaseapp.com',
  databaseURL: 'https://vue-todos-6ca0a.firebaseio.com',
  projectId: 'vue-todos-6ca0a',
  storageBucket: 'vue-todos-6ca0a.appspot.com',
  messagingSenderId: '1098173208241',
  appId: '1:1098173208241:web:3469b7a36d209f25d8f490'
};

firebase.initializeApp(firebaseConfig);
window.firebase = firebase;

let app;

firebase.auth().onAuthStateChanged(user => {
  if (!app) {
    store.commit('auth/setCurrentUser', user);
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app');
  }
});
